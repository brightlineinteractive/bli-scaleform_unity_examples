/**********************************************************************

Filename    :	Util.cs
Content     :  
Created     :   
Authors     :   Adam Petrone

Copyright   :   Copyright 2014 Autodesk, Inc. All Rights reserved.

Use of this software is subject to the terms of the Autodesk license
agreement provided at the time of installation or download, or which
otherwise accompanies this software in either electronic or hard copy form.
 
***********************************************************************/


using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.IO;
using UnityEngine;
using Scaleform;

public class Util
{
	/// <summary>
	/// Loads the file into memory as a Byte array.
	/// </summary>
	/// <returns>
	/// Byte array with file contents.
	/// </returns>
	/// <param name='filename'>
	/// A filename relative to the StreamingAssets folder.
	/// </param>
	static public Byte[] LoadFileToMemory(string filename)
	{
		string filePath = System.IO.Path.Combine(Application.streamingAssetsPath, filename);
#if UNITY_ANDROID
		if (filePath.Contains("jar:file://"))
		{
			WWW url = new WWW(filePath);
			
			// wait until 'download' finishes
			while(!url.isDone)
			{
			}

			return url.bytes;
		}
		else
#endif

		{
			Byte[] fileBytes = null;
			filePath = SFManager.GetScaleformContentPath() + filename;
			try
			{
				using (FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
				{
					using (BinaryReader reader = new BinaryReader(fileStream))
					{
						fileBytes = reader.ReadBytes((int)fileStream.Length);
					}
				}

				return fileBytes;
			}
			catch (Exception e)
			{
				Debug.Log(String.Format("Loading file failed with message = '{0}'", e.Message));
			}
		}
		
		return null;
	}
	
	/// <summary>
	/// Create and return a new movie instance with the given swfName.
	/// </summary>
	/// <returns>
	/// An instance of class T.
	/// </returns>
	/// <param name='swfName'>
	/// A path to the SWF or GFX file relative to the StreamingAssets folder.
	/// </param>
	/// <param name='fromByteStream'>
	/// If true, an attempt will be made to load this file into memory. On some platforms, this is useful when there isn't a real file path.
	/// </param>
	/// <param name='backgroundColor'>
	/// The background clear color for this movie.
	/// </param>
	/// <param name='renderTarget'>
	/// The RenderTarget to use when rendering this movie. A null value just renders to the screen.
	/// </param>
	/// <param name='scaleModeType'>
	/// Override the default ScaleModeType for this movie. Default is SM_ShowAll.
	/// </param>
	/// <typeparam name='T'>
	/// The class type that will be instanced for this movie.
	/// </typeparam> 
	public static T CreateSwf<T>(string swfName, bool fromByteStream, Color32 backgroundColor, RenderTexture renderTarget=null, ScaleModeType scaleModeType=ScaleModeType.SM_ShowAll, string movieName=null)
	{
		SFCamera camera = Component.FindObjectOfType(typeof(SFCamera)) as SFCamera;
		if (camera == null)
		{
			return default(T);
		}

		int depth = 1;
		bool initFirstFrame = false;
		bool useBackgroundColor = false;
		bool autoManageViewport = true;
		int ox = 0, oy = 0, width = 0, height = 0;
		Int32 length = 0;
		IntPtr unmanagedData = IntPtr.Zero;
		Byte[] swfBytes = null;
		
		// load the swf either from a byte array or via path
		SFMovieCreationParams creationParams;

		
		if (renderTarget != null)
		{
			ox = 0;
			oy = 0;
			width = renderTarget.width;
			height = renderTarget.height;

			useBackgroundColor = true;
			autoManageViewport = false;
		}
		else
		{
			SFCamera.GetViewport(ref ox, ref oy, ref width, ref height);
		}
	
		
		bool loadSucceeded = false;
		if (fromByteStream)
		{
			swfBytes = Util.LoadFileToMemory(swfName);
			if (swfBytes.Length > 0)
			{
				loadSucceeded = true;

				length = swfBytes.Length;
				unmanagedData = new IntPtr();
				unmanagedData = Marshal.AllocCoTaskMem((int)length);
				Marshal.Copy(swfBytes, 0, unmanagedData, (int)length);
			}
			else
			{
				Debug.Log("Unable to load swf from memory!");
			}
		}
		else
		{
			// no more to do here -- pass it on to the runtime.
			loadSucceeded = true;
		}
		

		string fullSwfPath = SFManager.GetScaleformContentPath() + swfName;
		creationParams = new SFMovieCreationParams(fullSwfPath, depth, ox, oy, width, height,
			unmanagedData, length, initFirstFrame, renderTarget, backgroundColor, useBackgroundColor, scaleModeType,
			autoManageViewport);


		if (movieName != null)
		{
			creationParams.MovieName = movieName;
		}

		if (loadSucceeded)
		{
			// create and return the instance
			T instance = (T)Activator.CreateInstance(typeof(T), camera, camera.GetSFManager(), creationParams);
			return instance;
		}
		else
		{
			Debug.Log(String.Format("Unable to load swf named: {0}", swfName));
			return default(T);
		}
	}

}
