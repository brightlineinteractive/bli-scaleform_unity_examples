﻿using UnityEngine;
using System.Collections;
using UnityEngine;
using Scaleform;

public class MyFlash : Movie {

	protected Value	theMovie = null;
	private FlashCamera parent = null;
	private Value flashText = null; 
	private bool clicked; 

	public MyFlash(FlashCamera parent, SFManager sfmgr, SFMovieCreationParams cp) :
		base(sfmgr, cp)
	{
		this.parent = parent;
		SFMgr = sfmgr;
		this.SetFocus(true);

	}
	
	
	public void OnRegisterSWFCallback(Value movieRef)
	{
		theMovie = movieRef;
	}

	public void OnBtn1Unity (string txt)
	{
		flashText = theMovie.GetMember ("TxtDisplay"); 
		flashText.SetMember ("text", txt); 
	}
	public void OnBtn2Unity ()
	{
		if(!clicked){
			clicked=true;
		}
		else{
			clicked=false; 
		}

		theMovie.Invoke ("OnReceivedFromUnity", clicked); 

	}
}
