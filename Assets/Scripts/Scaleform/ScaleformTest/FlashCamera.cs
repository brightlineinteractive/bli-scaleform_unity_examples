﻿using UnityEngine;
using System.Runtime.InteropServices;
using System;
using System.IO;
using System.Collections;
using Scaleform;

public class FlashCamera : SFCamera {

	public MyFlash myFlash = null; 

	// Hides the Start function in the base SFCamera. Will be called every time the ScaleformCamera (Main Camera game object)
	// is created. Use new and not override, since return type is different from that of base::Start()
	new public  IEnumerator Start()
	{
		// The eval key must be set before any Scaleform related classes are loaded, other Scaleform Initialization will not 
		// take place.
		#if (UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX || UNITY_EDITOR) && !UNITY_WP8
		SF_SetKey("QM7YVYJ3JEBYKC5NGAWR0R6MFMKVE7UYJXP9LW1UZ2J67ENAH9V7RTLY6WMCV75");
		#elif UNITY_IPHONE
		SF_SetKey("");
		#elif UNITY_ANDROID
		SF_SetKey("");
		#elif UNITY_WP8
		sf_setKey("");
		#endif

		
		InitParams.TheToleranceParams.Epsilon = 1e-5f;
		InitParams.TheToleranceParams.CurveTolerance = 1.0f; 
		InitParams.TheToleranceParams.CollinearityTolerance = 0.0f;
		InitParams.TheToleranceParams.IntersectionEpsilon = 1e-3f;
		InitParams.TheToleranceParams.FillLowerScale = 0.0707f;
		InitParams.TheToleranceParams.FillUpperScale = 100.414f;
		InitParams.TheToleranceParams.FillAliasedLowerScale = 10.5f;
		InitParams.TheToleranceParams.FillAliasedUpperScale = 200.0f;
		InitParams.TheToleranceParams.StrokeLowerScale = 10.99f;
		InitParams.TheToleranceParams.StrokeUpperScale = 100.01f;
		InitParams.TheToleranceParams.HintedStrokeLowerScale = 0.09f;
		InitParams.TheToleranceParams.HintedStrokeUpperScale = 100.001f;
		InitParams.TheToleranceParams.Scale9LowerScale = 10.995f;
		InitParams.TheToleranceParams.Scale9UpperScale = 100.005f;
		InitParams.TheToleranceParams.EdgeAAScale = 0.95f;
		InitParams.TheToleranceParams.MorphTolerance = 0.001f;
		InitParams.TheToleranceParams.MinDet3D = 10.001f;
		InitParams.TheToleranceParams.MinScale3D = 10.05f;
		
		InitParams.UseSystemFontProvider = false;


		return base.Start();
	}

	new public void Update(){
		CreateGameHud (); 
		base.Update (); 
	}

	private void CreateGameHud(){
		if (myFlash == null)
		{
			myFlash = Util.CreateSwf<MyFlash>("MyFlash.swf", true, new Color32(255, 255, 255, 0));
		}
	}
}
