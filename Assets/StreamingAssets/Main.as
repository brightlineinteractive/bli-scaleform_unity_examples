﻿package
{
	
	
	import flash.display.MovieClip;
	import flash.display.StageScaleMode; 
	import flash.display.StageAlign; 
	
	import flash.text.TextField;
	import flash.events.MouseEvent;
	import flash.external.*; 
	import flash.geom.ColorTransform;
	
	
	public class Main extends MovieClip 
	{

		public var TxtInput:TextField; 
		public var TxtDisplay:TextField; 
		public var Btn1:MovieClip; 
		public var Btn2:MovieClip; 
		
		private var Ct:ColorTransform = new ColorTransform(); 
		
		public function Main() 
		{
			stage.scaleMode=StageScaleMode.NO_SCALE; 
			
			Btn1.addEventListener(MouseEvent.CLICK,OnBtn1); 
			Btn2.addEventListener(MouseEvent.CLICK,OnBtn2); 
			
			ExternalInterface.call("OnRegisterSWFCallback", this);

		}
		
		private function OnBtn1 (e:MouseEvent):void
		{
			ExternalInterface.call("OnBtn1Unity", TxtInput.text); 
		}
		private function OnBtn2 (e:MouseEvent):void
		{
			ExternalInterface.call("OnBtn2Unity"); 
		}
		
		public function OnReceivedFromUnity (clicked:Boolean):void
		{
			if(!clicked){
				Ct.color = 0xCCCCFF; 
			}
			else{
				Ct.color = 0xCCFFFF; 
			}
			
			Btn1.transform.colorTransform = Ct; 
			trace(this, clicked); 
		}
	}
	
}
